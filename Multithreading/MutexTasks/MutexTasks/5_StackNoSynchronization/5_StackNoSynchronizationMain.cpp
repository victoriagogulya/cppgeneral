#include <iostream>
#include <vector>
#include <stack>
#include <thread>

void printAndRemove(std::stack<int>& s)
{
	if (!s.empty())
	{
		const int topElement = s.top();
		std::cout << "TopElement= " << topElement << " will be removed" << std::endl;
		s.pop();
	}
}

void main()
{
	std::stack<int> s;

	for (int i = 0; i < 11; i++)
	{
		s.push(i);
	}

	std::thread t1(printAndRemove, std::ref(s));
	std::thread t2(printAndRemove, std::ref(s));
	std::thread t3(printAndRemove, std::ref(s));

	t1.join();
	t2.join();
	t3.join();

	std::cout << "Now TopElement=" << s.top() << std::endl;
}