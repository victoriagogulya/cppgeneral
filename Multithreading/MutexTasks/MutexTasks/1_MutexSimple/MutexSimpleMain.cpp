#include <mutex>
#include <thread>
#include <chrono>
#include <iostream>


std::mutex mutex_global;

void foo()
{
	mutex_global.lock();

	std::cout << "entered thread_ID=" << std::this_thread::get_id() << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(rand() % 10));
	std::cout << "leaving thread_ID=" << std::this_thread::get_id() << std::endl;

	mutex_global.unlock();
}


//void main()
//{
//	srand((unsigned int)time(0));
//	std::thread t1(foo);
//	std::thread t2(foo);
//	std::thread t3(foo);
//
//	t1.join();
//	t2.join();
//	t3.join();
//}