Deadlock - ��������� �������� �� ���������.

-makeAppleJuice-
The input of makeAppleJuice is a bag which contains apples ("a") and pears ("p").
We also have two mutexes: m_print, which is associated to printing, and m_bag which is associated to the 
modification of the bag.
The function prints some information and removes the apples from the bag. (An apple is removed by changing the bag entry 
from "a" to "x"). Additionally, function counts the number of apples and makes the juice only if the number of apples 
is greater than 5.
The code for throwing out the pears is similar.

-throwOutPear-
The for loop removes all pears from the bag. A pear is removed from the bag by changing the bag entry 
from "p" to "x". Then, the function prints a messages. Of course, the mutexes protect the operations.

-main-
creates a bag and two threads for the two tasks. Finally, it also prints the content of the bag.
But when we try to run it, we might get an unexpected behavior. If everything goes well, the output is as following:
"Making an apple juice..."