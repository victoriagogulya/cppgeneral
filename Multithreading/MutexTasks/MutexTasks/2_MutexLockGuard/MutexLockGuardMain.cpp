#include <mutex>
#include <thread>
#include <chrono>
#include <iostream>


std::mutex mutex_global2;

void foo2()
{
	std::lock_guard<std::mutex> lock(mutex_global2);

	std::cout << "entered thread_ID=" << std::this_thread::get_id() << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(rand() % 10));
	std::cout << "leaving thread_ID=" << std::this_thread::get_id() << std::endl;
}


//void main()
//{
//	srand((unsigned int)time(0));
//	std::thread t1(foo2);
//	std::thread t2(foo2);
//	std::thread t3(foo2);
//
//	t1.join();
//	t2.join();
//	t3.join();
//}