
��������� ���������� ���������.

��� �� ����� �� ������������� ������������ ����� std::mutex ��������, 
���-��� ���� ����� �������� lock � unlock ����� ������������� ���������� - 
���������� deadlock (�.�. ��������������� ����� ��� � ��������� �����). 
�������� ������������ ���������� � C++ threading library ������ �������� ������� ��� C++ �������� - 
����������� ������� RAII (Resource Acquisition Is Initialization). 
�������� ������ ��������� ����� std::lock_guard. 
��� ������� �����, ����������� �������� �������� ����� lock ��� ��������� �������, 
� ���������� �������� unlock. 
����� � ����������� ������ std::lock_guard ����� �������� �������� std::adopt_lock - 
���������, ����������, ��� mutex ��� ������������ � ����������� ��� ������ �� ����. 
std::lock_guard �� �������� ������� ������ �������, ��� ������ ����������, ���������� ��� �����������.