#ifndef STACK_LIST
#define STACK_LIST

#include <list>
#include <algorithm>
#include <iostream>

#include "IStack.h"


template<typename T>
class StackList : public IStack<T>
{
public:
	~StackList() {}
	void push(const T& element) override;
	T pop() override;
	const T& peek() const override;

	bool empty() const override;
	int size() const override;
	bool contains(const T& element) const override;
	void clear() override;
	void print() const override;

private:
	std::list<T> m_data;
};


template<typename T>
inline void StackList<T>::push(const T& element)
{
	m_data.push_back(element);
}

template<typename T>
inline T StackList<T>::pop()
{
	auto element = m_data.back();
	m_data.pop_back();
	return element;
}

template<typename T>
inline const T& StackList<T>::peek() const
{
	return m_data.back();
}

template<typename T>
inline bool StackList<T>::empty() const
{
	return m_data.empty();
}

template<typename T>
inline int StackList<T>::size() const
{
	return m_data.size();
}

template<typename T>
inline bool StackList<T>::contains(const T& element) const
{
	return std::find(m_data.begin(), m_data.end(), element) != m_data.end();
}

template<typename T>
inline void StackList<T>::clear()
{
	m_data.clear();
}

template<typename T>
inline void StackList<T>::print() const
{
	std::cout << "Print Stack elements" << endl;
	for (const auto& element : m_data)
	{
		std::cout << element << endl;
	}
}
#endif //STACK_LIST