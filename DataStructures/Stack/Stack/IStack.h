#ifndef ISTACK_H
#define ISTACK_H


template <typename T>
struct IStack
{
	virtual void push(const T& element) = 0;
	virtual T pop() = 0;
	virtual const T& peek() const = 0;

	virtual bool empty() const = 0;
	virtual int size() const = 0;
	virtual bool contains(const T& element) const = 0;
	virtual void clear() = 0;
	virtual void print() const = 0;

	virtual ~IStack() {}
};


#endif //ISTACK_H
