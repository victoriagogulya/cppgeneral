﻿#include <iostream>
using namespace std;

#include <vector>

#include "StackVector.h"
#include "StackList.h"


void runStackOperations(IStack<int>& st);


int main()
{
	StackVector<int> st1;
	runStackOperations(st1);

	StackList<int> st2;
	runStackOperations(st2);

	return 0;
}

void runStackOperations(IStack<int>& st)
{
	cout << "\nPush elemnts to stack: 1,2,3,4,5" << endl;
	for (int i = 1; i < 6; ++i)
	{
		st.push(i);
	}
	st.print();
	cout << "\nSize of stack is " << st.size() << endl;
	cout << "Stack contains element 5: " << (st.contains(5) ? "True" : "False") << endl;
	cout << "Stack doesn't contain element 8: " << (st.contains(8) ? "True" : "False") << endl;

	cout << "\nPop 2 elements" << endl;
	st.pop();
	st.pop();
	st.print();

	cout << "\nPeek shows the last element: 3" << endl;
	cout << st.peek() << endl;

	cout << "\nClear Stack" << endl;
	st.clear();
	cout << "Stack is empty now: " << (st.empty() ? "True" : "False") << endl;
}
