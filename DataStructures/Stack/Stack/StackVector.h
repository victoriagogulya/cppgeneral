#ifndef STACK_VECTOR
#define STACK_VECTOR

#include <vector>
#include <algorithm>
#include <iostream>

#include "IStack.h"


template<typename T>
class StackVector : public IStack<T>
{
public:
	~StackVector(){}
	void push(const T& element) override;
	T pop() override;
	const T& peek() const override;

	bool empty() const override;
	int size() const override;
	bool contains(const T& element) const override;
	void clear() override;
	void print() const override;

private:
	vector<T> m_data;
};


template<typename T>
inline void StackVector<T>::push(const T& element)
{
	m_data.push_back(element);
}

template<typename T>
inline T StackVector<T>::pop()
{
	auto element = m_data.back();
	m_data.pop_back();
	return element;
}

template<typename T>
inline const T& StackVector<T>::peek() const
{
	return m_data.back();
}

template<typename T>
inline bool StackVector<T>::empty() const
{
	return m_data.empty();
}

template<typename T>
inline int StackVector<T>::size() const
{
	return m_data.size();
}

template<typename T>
inline bool StackVector<T>::contains(const T& element) const
{
	return std::find(m_data.begin(), m_data.end(), element) != m_data.end();
}

template<typename T>
inline void StackVector<T>::clear()
{
	m_data.clear();
}

template<typename T>
inline void StackVector<T>::print() const
{
	std::cout << "Print Stack elements" << endl;
	for (const auto& element : m_data)
	{
		std::cout << element << endl;
	}
}

#endif //SATCK_VECTOR