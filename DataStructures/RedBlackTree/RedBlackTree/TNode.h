#ifndef _TNODE_H_
#define _TNODE_H_

#include <memory>

template <typename TKey, typename TValue>
struct TNode
{
	TKey                      mKey;
	TValue                    mValue;
	std::shared_ptr<TNode>    mLeft;
	std::shared_ptr<TNode>    mRight;
	bool                      mIsRed;

	TNode();
	TNode(const TKey& key, const TValue& value);
};

template <typename TKey, typename TValue>
TNode<TKey, TValue>::TNode() :
mLeft(nullptr)
, mRight(nullptr)
, mIsRed(true)
{

}

template <typename TKey, typename TValue>
TNode<TKey, TValue>::TNode(const TKey& key, const TValue& value) :
mKey(key)
, mValue(value)
, mLeft(nullptr)
, mRight(nullptr)
, mIsRed(true)
{

}

#endif //_TNODE_H_