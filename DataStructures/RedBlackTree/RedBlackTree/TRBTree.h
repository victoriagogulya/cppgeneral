#ifndef _TRBTREE_H_
#define _TRBTREE_H_

#include "TNode.h"
#include <iostream>
using namespace std;


template <typename TKey, typename TValue>
class TRBTree
{
public:
	TRBTree();
	explicit TRBTree(const std::shared_ptr<TNode<TKey, TValue>>& root);
	~TRBTree();

	std::shared_ptr<TNode<TKey, TValue>> find(const TKey& key) const;
	void insert(const TKey& key, const TValue& value);
	void deleteMin();
	void remove(const TKey& key);

	void traversePreOrder() const;
	void traverseInOrder() const;
	void traversePostOrder() const;

private:
	TRBTree(const TRBTree<TKey, TValue>& tree) = delete;
	TRBTree& operator=(const TRBTree<TKey, TValue>& tree) = delete;

	bool isRed(const std::shared_ptr<TNode<TKey, TValue>>& node) const;
	std::shared_ptr<TNode<TKey, TValue>> rotateLeft(std::shared_ptr<TNode<TKey, TValue>>& node);
	std::shared_ptr<TNode<TKey, TValue>> rotateRight(std::shared_ptr<TNode<TKey, TValue>>& node);
	void flipColors(std::shared_ptr<TNode<TKey, TValue>>& node);

	std::shared_ptr<TNode<TKey, TValue>> insert(std::shared_ptr<TNode<TKey, TValue>>& node, const TKey& key, const TValue& value);
	std::shared_ptr<TNode<TKey, TValue>> deleteMin(std::shared_ptr<TNode<TKey, TValue>>& node);
	std::shared_ptr<TNode<TKey, TValue>> remove(std::shared_ptr<TNode<TKey, TValue>>& node, const TKey& key);
	std::shared_ptr<TNode<TKey, TValue>> minNode(std::shared_ptr<TNode<TKey, TValue>>& node);

	void printNode(const std::shared_ptr<TNode<TKey, TValue>>& node) const;
	void traversePreOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const;
	void traverseInOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const;
	void traversePostOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const;

	std::shared_ptr<TNode<TKey, TValue>> mRoot;
};


template <typename TKey, typename TValue>
TRBTree<TKey, TValue>::TRBTree() : 
  mRoot(nullptr)
{

}

template <typename TKey, typename TValue>
TRBTree<TKey, TValue>::TRBTree(const std::shared_ptr<TNode<TKey, TValue>>& root) :
  mRoot(root)
{

}

template <typename TKey, typename TValue>
TRBTree<TKey, TValue>::~TRBTree()
{
	mRoot.reset();
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::find(const TKey& key) const
{
	std::shared_ptr<TNode<TKey, TValue>> node = mRoot;

	while (nullptr != node)
	{
		if      (key < node->mKey) { node = node->mLeft; }
		else if (key > node->mKey) { node = node->mRight; }
		else    return node;
	}
	
	node = nullptr;
	return node;
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::insert(const TKey& key, const TValue& value)
{
	mRoot = insert(mRoot, key, value);
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::deleteMin()
{
	mRoot = deleteMin(mRoot);
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::remove(const TKey& key)
{
	mRoot = remove(mRoot, key);
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traversePreOrder() const
{
	cout << "----- traversePreOrder -----" << endl;
	traversePreOrder(mRoot);
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traverseInOrder() const
{
	cout << "----- traverseInOrder -----" << endl;
	traverseInOrder(mRoot);
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traversePostOrder() const
{
	cout << "----- traversePostOrder -----" << endl;
	traversePostOrder(mRoot);
}

template <typename TKey, typename TValue>
bool TRBTree<TKey, TValue>::isRed(const std::shared_ptr<TNode<TKey, TValue>>& node) const
{
	return ((nullptr != node) && (true == node->mIsRed));
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::rotateLeft(std::shared_ptr<TNode<TKey, TValue>>& node)
{
	std::shared_ptr<TNode<TKey, TValue>> nodeNew = node->mRight;
	node->mRight = nodeNew->mLeft;
	nodeNew->mLeft = node;
	nodeNew->mIsRed = node->mIsRed;
	node->mIsRed = true;
	return nodeNew;
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::rotateRight(std::shared_ptr<TNode<TKey, TValue>>& node)
{
	std::shared_ptr<TNode<TKey, TValue>> nodeNew = node->mLeft;
	node->mLeft = nodeNew->mRight;
	nodeNew->mRight = node;
	nodeNew->mIsRed = node->mIsRed;
	node->mIsRed = true;
	return nodeNew;
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::flipColors(std::shared_ptr<TNode<TKey, TValue>>& node)
{
	node->mIsRed = true;
	node->mLeft->mIsRed = false;
	node->mRight->mIsRed = false;
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::insert(std::shared_ptr<TNode<TKey, TValue>>& node, const TKey& key, const TValue& value)
{
	if (node == nullptr) { return std::make_shared<TNode<TKey, TValue>>(key, value); }

	if      (key < node->mKey) { node->mLeft = insert(node->mLeft, key, value); }
	else if (key > node->mKey) { node->mRight = insert(node->mRight, key, value); }
	else     node->mValue = value;
	
	if (isRed(node->mRight) && !isRed(node->mLeft))        { node = rotateLeft(node); }
	if (isRed(node->mLeft)  &&  isRed(node->mLeft->mLeft)) { node = rotateRight(node); }
	if (isRed(node->mRight) &&  isRed(node->mLeft))        { flipColors(node); }
	
	return node;
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::deleteMin(std::shared_ptr<TNode<TKey, TValue>>& node)
{
	if (nullptr == node->mLeft) { return node->mRight; }
	node->mLeft = deleteMin(node->mLeft);
	return node;
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> remove(std::shared_ptr<TNode<TKey, TValue>>& node, const TKey& key)
{
	if (node == nullptr) { return node; }
	if      (key < node->mKey) { node->mLeft = remove(node->mLeft, key); }
	else if (key > node->mKey) { node->mRight = remove(node->mRight, key); }
	else
	{
		if (nullptr == node->mRight) { return node->mLeft; }
		if (nullptr == node->mLeft) { return node->mRight; }

		auto nodeTmp = node;
		node = min(nodeTmp->mRight);
		node->mRight = deleteMin(nodeTmp->mRight);
		node->mLeft = nodeTmp->mLeft;
	}

	if (isRed(node->mRight) && !isRed(node->mLeft))        { node = rotateLeft(node); }
	if (isRed(node->mLeft) && isRed(node->mLeft->mLeft)) { node = rotateRight(node); }
	if (isRed(node->mRight) && isRed(node->mLeft))        { flipColors(node); }

	return node;
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::printNode(const std::shared_ptr<TNode<TKey, TValue>>& node) const
{
	if (node != nullptr)
	{
		cout << "[" << node->mKey << "]=" << node->mValue << endl;
	}
	else
	{
		cout << "Node is null!" << endl;
	}
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traversePreOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const
{
	if (node != nullptr)
	{
		printNode(node);
		traversePreOrder(node->mLeft);
		traversePreOrder(node->mRight);
	}
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traverseInOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const
{
	if (node != nullptr)
	{
		traverseInOrder(node->mLeft);
		printNode(node);
		traverseInOrder(node->mRight);
	}
}

template <typename TKey, typename TValue>
void TRBTree<TKey, TValue>::traversePostOrder(const std::shared_ptr<TNode<TKey, TValue>>& node) const
{
	if (node != nullptr)
	{
		traversePostOrder(node->mLeft);
		traversePostOrder(node->mRight);
		printNode(node);
	}
}

template <typename TKey, typename TValue>
std::shared_ptr<TNode<TKey, TValue>> TRBTree<TKey, TValue>::minNode(std::shared_ptr<TNode<TKey, TValue>>& node)
{
	if (node->mLeft == nullptr) { return node; }
	else { return min(node->mLeft); }
}

#endif //_TRBTREE_H_