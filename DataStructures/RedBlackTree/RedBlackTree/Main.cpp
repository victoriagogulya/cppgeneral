#include "TRBTree.h"
#include <iostream>
using namespace std;


void main()
{
	auto tree = std::make_shared<TRBTree<char, int>>();
	tree->insert('S', 7);
	tree->insert('E', 7);
	tree->insert('A', 7);
	tree->insert('R', 7);
	tree->insert('C', 7);
	tree->insert('H', 7);
	tree->insert('X', 7);
	tree->insert('M', 7);
	tree->insert('P', 7);
	tree->insert('L', 7);

	//tree->traversePreOrder();
	tree->traverseInOrder();
	//tree->traversePostOrder();

	tree->remove('A');
	tree->traverseInOrder();
}