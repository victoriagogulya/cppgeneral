#include "MatrixFunctions.h"


void main()
{
	TMatrix<bool> matrixAdjacency;

	matrixAdjacency.push_back(vector<bool>{ 0, 1, 0, 1, 0 });
	matrixAdjacency.push_back(vector<bool>{ 0, 0, 0, 0, 0 });
	matrixAdjacency.push_back(vector<bool>{ 1, 1, 0, 0, 0 });
	matrixAdjacency.push_back(vector<bool>{ 0, 0, 1, 0, 0 });
	matrixAdjacency.push_back(vector<bool>{ 1, 0, 0, 1, 0 });

	cout << "Adjacency matrix" << endl;
	printMatrix(matrixAdjacency);

	TMatrix<bool> matrixReachability = getReachabilityMatrix(matrixAdjacency);
	cout << "Reachability matrix" << endl;
	printMatrix(matrixReachability);

	cout << "StrongCoupling matrix" << endl;
	TMatrix<bool> matrixStrongCoupling = getStrongCouplingMatrix(matrixAdjacency);
	printMatrix(matrixStrongCoupling);
}