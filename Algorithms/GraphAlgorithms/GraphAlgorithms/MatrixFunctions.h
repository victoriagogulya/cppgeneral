#ifndef MATRIX_FUNCTIONS_H
#define MATRIX_FUNCTIONS_H

#include<vector>
#include <iostream>
using namespace std;

template<typename T>
using TMatrix = vector<vector<T>>;

template<typename T>
void printMatrix(const TMatrix<T>& matrix)
{
	for (const auto& line : matrix)
	{
		for (const auto& element : line)
		{
			cout << element << ' ';
		}
		cout << endl;
	}
	cout << endl;
}

template<typename T>
TMatrix<T> getReachabilityMatrix(const TMatrix<T>& matrixAdjacency)
{
	TMatrix<T> matrix;

	for (size_t i = 0; i < matrixAdjacency.size(); i++)
	{
		vector<T> line;
		
		for (size_t j = 0; j < matrixAdjacency[i].size(); j++)
		{
			line.push_back((i == j) || (matrixAdjacency[i][j] == 1));
		}
		matrix.push_back(line);
	}

	for (size_t vertex = 0; vertex < matrix.size(); vertex++)
	{
		for (size_t i = 0; i < matrix.size(); i++)
		{
			for (size_t j = 0; j < matrix[i].size(); j++)
			{
				if (matrix[i][j] == 0)
				{
					if (matrix[vertex][j] == 1 && matrix[i][vertex] == 1)
					{
						matrix[i][j] = 1;
					}
				}	
			}
		}
	}

	return matrix;
}

template<typename T>
bool isPath(const int coordY, const int coordX, const TMatrix<T>& matrixAdjacency)
{
	TMatrix<T> matrixReachability = getReachabilityMatrix(matrixAdjacency);

	return matrixReachability[coordY][coordX];
}

template<typename T>
TMatrix<T> getStrongCouplingMatrix(const TMatrix<T>& matrixAdjacency)
{
	TMatrix<T> matrix = getReachabilityMatrix(matrixAdjacency);

	for (size_t i = 0; i < matrix.size(); i++)
	{
		for (size_t j = 0; j < matrix[i].size(); j++)
		{
			matrix[i][j] = (matrix[i][j] == 1) && (matrix[j][i] == 1);
		}
	}

	return matrix;
}

#endif //MATRIX_FUNCTIONS_H