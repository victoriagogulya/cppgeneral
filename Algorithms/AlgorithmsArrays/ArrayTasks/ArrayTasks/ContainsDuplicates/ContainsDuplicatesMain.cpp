#include "../Utils.h"
#include <set>

/*
Given an array of integers, find if the array contains any duplicates. 
Your function should return true if any value appears at least twice in the array, 
and it should return false if every element is distinct.
*/

bool containsDuplicate(vector<int>& nums)
{
	set<int> s(nums.begin(), nums.end());

	return (s.size() < nums.size());

}

//void main()
//{
//	vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
//
//	cout << containsDuplicate(v);
//}