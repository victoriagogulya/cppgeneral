#include <iostream>
using namespace std;
#include <vector>
#include "../Utils.h"

/*
Rotate an array of n elements to the right by k steps.
For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].

Note:
Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.

Hint:
Could you do it in-place with O(1) extra space?
*/

int computeRealK(int size, int k)
{
	int loops = k / size;
	int countInLoops = loops * size;
	int index = k - countInLoops;

	return index;
}

void rotate(vector<int>& nums, int k) {

	const int size = nums.size();
	const int realK = computeRealK(size, k);

	int index = 0;
	int value = nums[index];

	for (int i = 0, counter = 0; i < size; i++)
	{
		int valueCurrent = value;

		index += realK;
		index = (index < size) ? index : index - size;
		value = nums[index];
		nums[index] = valueCurrent;
		NUtils::printVector(nums);

		if (index <= counter)
		{
			++counter;
			++index;
			value = nums[index];
		}
	}
}

//void main()
//{
//	vector<int> v{ 1, 2, 3, 4, 5, 6, 7 };
//	int k = 10;
//
//	NUtils::printVector(v);
//	rotate(v, k);
//	NUtils::printVector(v);
//}