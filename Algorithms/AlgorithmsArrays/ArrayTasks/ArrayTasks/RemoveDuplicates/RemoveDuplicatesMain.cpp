#include "../Utils.h"


int removeDuplicates(vector<int>& nums) {
	for (int i = 0; i < nums.size(); i++)
	{
		for (int j = i + 1; j < nums.size(); j++)
		{
			if (nums[i] == nums[j])
			{
				nums.erase(nums.begin() + j);
				j--;
			}
		}
	}
	return nums.size();
}

//void main()
//{
//	vector<int> v{ 1, 7, 2, 3, 1, 4, 4, 5, 6, 7 };
//
//	NUtils::printVector(v);
//	cout << "size=" << v.size() << endl;
//
//	cout << "-------RemoveDuplicates--------" << endl;
//	removeDuplicates(v);
//	NUtils::printVector(v);
//	cout << "size=" << v.size() << endl;
//}