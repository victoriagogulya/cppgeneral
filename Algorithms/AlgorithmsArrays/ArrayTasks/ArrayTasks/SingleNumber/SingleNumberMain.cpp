#include "../Utils.h"
#include <algorithm>

int singleNumber(vector<int>& nums) 
{
	int number = nums[0];
	std::sort(nums.begin(), nums.end());

	for (int i = 0; i < nums.size(); i += 2)
	{
		if (i + 1 < nums.size())
		{
			if (nums[i] != nums[i + 1])
			{
				number = nums[i];
				break;
			}
		}
		else
		{
			number = nums[i];
		}
	}

	return number;
}

void main()
{
	vector<int> v = { 1, 4, 5, 6, 6, 4, 1 };

	cout << singleNumber(v);
}