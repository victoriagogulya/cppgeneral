#ifndef _UTILS_H_
#define _UTILS_H_

#include <iostream>
using namespace std;
#include <vector>

namespace NUtils
{
	template<typename T>
	void printVector(const vector<T>& v)
	{
		cout << "printVector" << endl;
		for (auto element : v)
		{
			cout << element << ' ';
		}
		cout << endl;
	}
}

#endif //_UTILS_H_