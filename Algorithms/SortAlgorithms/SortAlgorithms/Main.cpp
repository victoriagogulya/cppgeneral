#include <vector>
#include <list>
#include <algorithm>
#include <random>
#include <math.h>
#include <iostream>
using namespace std;

#include "Algorithms.h"


int main()
{
	std::vector<int> v;
	std::vector<int> vInsertion;
	std::vector<int> vShell;

	//Insertion sort
	std::cout << "\n\nInput - sorted in acsending order vector" << endl;
	v = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	vInsertion = v;
	vShell = v;
	alg::insertionSort(vInsertion);
	alg::shellSort(vShell);

	std::cout << "\n\nInput - sorted in descending order vector" << endl;
	v = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
	vInsertion = v;
	vShell = v;
	alg::insertionSort(vInsertion);
	alg::shellSort(vShell);

	std::cout << "\n\nInput - random shuffled vector, size=10" << endl;
	v = alg::generateRandom(10, 0, 100);
	vInsertion = v;
	vShell = v;
	alg::insertionSort(vInsertion);
	alg::shellSort(vShell);

	std::cout << "\n\nInput - random shuffled vector, size=10^2" << endl;
	v = alg::generateRandom(100, 0, 100);
	vInsertion = v;
	vShell = v;
	alg::insertionSort(vInsertion);
	alg::shellSort(vShell);

	std::cout << "\n\nInput - random shuffled vector, size=10^3" << endl;
	v = alg::generateRandom(pow(100,2), 0, 100);
	vInsertion = v;
	vShell = v;
	alg::insertionSort(vInsertion);
	alg::shellSort(vShell);

	return 0;
}