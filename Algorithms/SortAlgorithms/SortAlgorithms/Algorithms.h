#ifndef _ALGORITHMS_H
#define _ALGORITHMS_H

#include <random>
#include <algorithm>
#include <iterator>
#include <iostream>
#include <functional>
#include <iostream>
using namespace std;

namespace alg
{
	template<typename T>
	void swap(T& x, T& y)
	{
		T z = std::move(x);
		x = std::move(y);
		y = std::move(z);
	}

	template<typename TIterator>
	void print(TIterator& first, TIterator& last)
	{
		for (; first != last; first++)
		{
			cout << *first;
		}
		cout << endl;
	}

	template<typename T>
	void printVector(std::vector<T>& v)
	{
		cout << "----- printVector size=" << v.size() << "----- " << endl;
		for (auto& element : v)
		{
			cout << element << ' ';
		}
		cout << endl;
	}

	void printSwapsNumder(const int swapsNumber, const int vectorSize)
	{
		cout << "Number of elements: " << vectorSize << endl;
		cout << "Number of swaps: " << swapsNumber << endl;
	}

	template<typename T>
	std::vector<T> generateRandom(const int valsNumber, const T startVal, const T endVal)
	{
		// First create an instance of an engine.
		random_device rnd_device;

		// Specify the engine and distribution.
		mt19937 mersenne_engine{ rnd_device() };  

		// Generates random integers
		uniform_int_distribution<T> dist{ startVal, endVal };

		auto gen = [&dist, &mersenne_engine]() {
			return dist(mersenne_engine);
		};

		std::vector<T> v(valsNumber);
		generate(v.begin(), v.end(), gen);

		return v;
	}

	//Bubble Sort
	template<typename T>
	void bubbleSort(std::vector<T>& v)
	{
		for (int i = 0; i < v.size() - 1; i++)
		{
			bool swapped = false;
			for (int j = 0; j < v.size() - i - 1; j++)
			{
				if (v[j] > v[j + 1]) {
					swap(v[j], v[j + 1]);
					swapped = true;
				}
				printVector(v);
			}
			cout << endl;

			if (!swapped)
				break;
		}
	}

	//Insertion Sort
	template<typename T>
	void insertionSort(std::vector<T>& v, const bool verbose = false)
	{
		cout << "Insertion sort" << endl;
		if (verbose) printVector(v);

		int counter = 0;
		for (auto i = 1; i < v.size(); i++)
		{
			for (auto j = i; j > 0 && (v[j - 1] > v[j]); j--)
			{
				swap(v[j - 1], v[j]);
				counter++;
			}
		}
		if (verbose) printVector(v);
		printSwapsNumder(counter, v.size());
	}

	//Shell Sort
	template<typename T>
	void shellSort(std::vector<T>& v, const bool verbose = false)
	{
		cout << "Shell sort" << endl;
		if (verbose) printVector(v);
		auto length = v.size();
		int h = 1;
		int counter = 0;

		while (h < length / 3) { h = 3 * h + 1; }
		cout << "h: " << h << endl;

		while (h >= 1)
		{
			//h-sort the array
			for (int i = h; i < length; i++)
			{
				for (int j = i; j >= h && v[j] < v[j - h]; j -= h)
				{
					swap(v[j], v[j - h]);
					counter++;
				}
			}
			h = h / 3;
		}

		if (verbose) printVector(v);
		printSwapsNumder(counter, v.size());
	}

	template<typename T>
	void combSort(std::vector<T>& v)
	{
		double fakt = 1.2473309; // ������ ����������
		double step = v.size() - 1;

		while (step >= 1)
		{
			for (int i = 0; i + step < v.size(); ++i)
			{
				if (v[i] > v[i + step])
				{
					swap(v[i], v[i + step]);
				}
				printVector(v);
			}
			step /= fakt;
		}

		bubbleSort(v);
	}

	template<typename TIterator>
	void bubbleSort2(TIterator& first, TIterator& last)
	{
		TIterator firstTmp;
		TIterator lastTmp;
		//lastTmp < firstTmp;
		/*
		for (firstTmp = first; firstTmp != last; firstTmp++)
		{
			for (lastTmp = first; lastTmp < firstTmp; lastTmp++)
			{
				if (*firstTmp < *lastTmp)
				{
					std::iter_swap(firstTmp, lastTmp);
				}
			}
		}*/
	}
}

#endif //_ALGORITHMS_H